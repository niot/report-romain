
\chapter{Additional instructions for replication}

% add context
We, Paulette and Guilhem, have succeeded in replicating the setup of Romain. In this appendix, we clarify some of the installation steps, and extend some of the results. In the following sections, we first present these clarifications, and then we present new materials on: 1) using alternative clients; 2) the range that can be expected of Wifi connection; 3) additional ethernet connexion on Windows and Android; and 4) a procedure to deactivate the GUI to make the Raspberry Pi startup faster.

%TODO 
\section{Initial Configuration of the Raspberry Pi}

In Section \ref{chap:tools}, Romain presents how to install the missing libraries but it may not be clear how you access the Raspberry Pi in the first place.
To do so, you have two options, either by systematically attaching a keyboard and screen directly to the device or by SSH-ing while it is connected to the same network.

\begin{itemize}
    \item On the one hand, connecting a screen is simple if you have the required cables. But it may result in a poor experience as rendering the Desktop is resource demanding, and you need to navigate between two devices.
    \item On the other hand, using SSH allows you to do all your manipulations on the same computer. But SSH needs to be enabled the first time you use the Raspberry Pi. It can be enabled either using a SD card reader and by creating an empty file named \emph{ssh} in the folder \emph{/boot}, or SSH can be enabled by plugging a screen and keyboard once, opening a terminal and running \emph{raspi-config}\footnote{\emph{raspi-config} is included by default in RaspOS, see \url{https://elinux.org/RPi_raspi-config}}, which is an interactive command offering the option to activate SSH on the Raspberry Pi. \\
    You will then be able to connect to the Raspberry Pi using \emph{ssh pi@raspberrypi.local} with password \emph{raspberry} (default password) when connected to the same network as the Raspberry Pi, or when connected with Ethernet over USB\footnote{See \autoref{ethernet-over-usb} for configuring it.}. \\

    \textbf{Note:} In case the domain \emph{raspberrypi.local} is not resolved (for example if your Linux distribution is not including a discovery service for local domains\footnote{You can look for a library providing mDNS support in your distribution if you are in that situation.}), you can retrieve the IP address of your Raspberry Pi using \verb$arp -na | grep 'at b8:27:eb:'$ (this command retrieves all the IPs exposed on your local network associated to a MAC address prefixed with \verb|b8:27:eb:|; a prefix that is shared between all Raspberry Pis), and then use this IP address instead of the domain \emph{raspberrypi.local}.
\end{itemize}

\section{An Internet Connexion is Necessary}
\label{internetconnexion}

The package manager requires an internet connexion to install SSB and its dependencies. To do so, you can connect the Raspberry Pi to a Wi-Fi by appending Configuration \ref{conf:network_template} (\autoref{chap:conf}) to the file \verb|/etc/wpa_supplicant/wpa_supplicant.conf|. In case the original password is not working for you, you may need to hash the password using \emph{wpa\_passphrase}.
As the EPFL network is restricted and do not allow broadcast to peers on the local network, we used our phones as hotspot to obtain a more suited local network. \\

\section{Quickly Configuring the Raspberry Pi's Network}


While this was not mentioned in the original report, Romain created a script to ease the configuration of the network (corresponding to Section \ref{chap:impl}). The script creates and configures the services for the networking, and for interacting with the Raspberry using the buttons and LEDs. It can be used by first cloning \url{https://gitlab.epfl.ch/sacs/ssb/smallworld/gossiping-locally} and then executing \verb|install.sh|.


\section{Demo with Manyverse}

When using a single Raspberry Pi and a single client (computer with OASIS, from \autoref{configuration}) it's difficult to know when the data has been replicated.
As smartphones are now quite common, we solved that problem using Manyverse as another client in addition to OASIS.
Manyverse \footnote{Download the app here: \url{https://www.manyver.se/}} is a social network mobile app that uses the SSB protocol (Secure Scuttlebutt).
You can write posts and share them with friends through a shared local Wi-Fi or on the Internet.

However, Manyverse requires a different approach than what was described in Section \ref{wps} since WPS is no longer supported on smartphones. If the Raspberry Pi is already connected to the hotspot created by your phone as explained in Section \ref{internetconnexion}, then you have nothing else to do. Otherwise you can connect your smartphone to your Raspberry Pi by manually typing the Wi-Fi password on your smartphone. 
In that case, the default password setup by the \emph{install.sh} script is \emph{SmallNetpw}. 

\section{Wi-Fi Network Range}
We did preliminary testing to measure the Wi-Fi range when using the Raspberry Pi.
We did two tests to define the approximate range of the network in meters. These tests were carried out in the BC building of EPFL. For both tests, the Raspberry Pi stayed in office \#163 on the first floor of the building, and we moved with a smartphone connected to the Raspberry Pi to see how far we could go without losing the signal.

For the first experiment, we moved to office \#102 with the smartphone, which is approximately 31 meters away from the starting point. If we moved further we lost the signal.
For the second test, we wanted to see whether the signal held between floors. We went up to the fourth floor and the signal was even stronger than the first experiment. 
We suppose that this is because in the first experiment there were at least 5 intermediate walls interrupting the signal.

The specifications of the Raspberry Pi Zero W \footnote{\url{https://www.raspberrypi.org/products/raspberry-pi-zero-w/}} states that the Wi-Fi standard it uses is \emph{802.11 b/g/n wireless LAN}.
In a typical office environment, the connexion range should be from 30 at the highest speed setting up to 75 meters at the lowest \footnote{\url{https://www.blackbox.co.uk/gb-gb/page/24953/Resources/Technical-Resources/Black-Box-Explains/Wireless/wireless-standards}}. 
Our experiments confirmed this specification.

\section{Ethernet-over-USB}
We succeeded in replicating \autoref{ethernet-over-usb} of the report, which allowed us to SSH on the device. 
Since it worked well in a Linux laptop we wanted to test it in a Windows 10 laptop and an Android smartphone, but we found that it isn't as easy as plugging a USB cable into the device. 
So we are showing in the next two subsections how to SSH to the Raspberry Pi from both Windows 10 and Android.
However, we didn't managed to replicate data on SSB through the Internet over USB connection, so we are leaving this as future work.

\subsection{Windows OS}
When trying to connect a Raspberry Pi Zero via the USB port, on later builds of Windows 10, the connection only shows up as a COM port and not as a USB Ethernet/RNDIS Gadget, as illustrated in \autoref{fig:com-port}. 
We needed to follow some steps to make it work, which are listed below.

\begin{enumerate}
    \item Plug in the Raspberry Pi into the Windows 10 laptop.
    \item Go to the Device Manager, you should find the Raspberry Pi as a \emph{COM port} under \emph{Ports (COM \& LPT)} section. Refer to \autoref{fig:com-port}.
    \item Download the updated driver\footnote{Download the driver at \url{https://www.catalog.update.microsoft.com/Search.aspx?q=usb\%5Cvid_0525\%26pid_a4a2}}. Select the one for Windows 8.1 and later, as showed in \autoref{fig:driver}.
    \item Extract the drivers from the CAB file you downloaded in Step 3, by double clicking the CAB file, then drag and drop the drivers on the desktop. Refer to Figures \ref{fig:cab-file} and \ref{fig:drivers-to-desktop}.
    \item Update the driver. You can refer to Figures \ref{fig:port-properties}, \ref{fig:update-driver} and \ref{fig:browse-driver} to follow the process.
    \begin{enumerate}
        \item Right click on COM3 port.
        \item Select \emph{Properties} option.
        \item Select \emph{Update Driver} option.
        \item Select \emph{Browse my computer for drivers} option.
        \item Select the location of the drivers you downloaded in Step 3.
    \end{enumerate}
    \item The Raspberry Pi will now show as \emph{USB Ethernet/RNDIS Gadget} under \emph{Network adapters} section, as illustrated in \autoref{fig:ethernet-over-usb-enabled}.
\end{enumerate}

After completing these steps you should be able to connect your Windows 10 laptop to the Raspberry Pi via SSH, as illustrated in the \autoref{fig:connected-windows}.

\begin{figure}[b]
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/com-port.png}
      \caption{COM3 port}
      \label{fig:com-port}  
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/driver.png}
      \caption{USB/Ethernet RNDIS driver}
      \label{fig:driver}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/properties.png}
      \caption{Port properties}
      \label{fig:port-properties}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/update-driver.png}
      \caption{Update driver}
      \label{fig:update-driver}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/browse-driver.png}
      \caption{Browse driver}
      \label{fig:browse-driver}  
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth, height=6.17cm]{figures/usb-working.png}
      \caption{Ethernet-over-USB enabled}
      \label{fig:ethernet-over-usb-enabled}
    \end{subfigure}
    \caption{Enabling Ethernet-over-USB on Windows 10 laptop (pt1)}
    \label{fig:ethernet-over-usb-windows10-pt1}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/cab-file.png}
    \caption{CAB file}
    \label{fig:cab-file}  
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/drivers-to-desktop.png}
    \caption{Moving drivers to desktop}
    \label{fig:drivers-to-desktop}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/connected-windows.png}
    \caption{Windows 10 laptop connected}
    \label{fig:connected-windows}
  \end{subfigure}
  \caption{Enabling Ethernet-over-USB on Windows 10 laptop (pt2)}
  \label{fig:ethernet-over-usb-windows10-pt2}
\end{figure}

\subsection{Android}
To be able to connect the Raspberry Pi via a USB port to an Android smartphone, in contrast to the instructions for Windows of the previous section, you don’t need to make any configuration in the device. 
Instead, follow these steps:

\begin{enumerate}
  \item Plug the USB cable into your Android smartphone.
  \item Enable \emph{USB Tethering}. Refer to Figures \ref{fig:setting}, \ref{fig:wireless-networks} and \ref{fig:tethering}.
  \begin{enumerate}
    \item Go to \emph{Settings} section.
    \item Go to \emph{Wireless \& networks} section.
    \item Go to \emph{Tethering \& portable hotspot} section.
    \item Enable \emph{USB tethering}.
  \end{enumerate}
  \item Find the IP of the Raspberry Pi. To do so, for example, you can install the \emph{Ping \& Net} app from the Play Store, as illustrated in \autoref{fig:ping-net-play-store}. Use the app as illustrated in Figures \ref{fig:ping-net} and \ref{fig:find-network}:
  \begin{enumerate}
    \item Open \emph{Ping \& Net} app.
    \item Click on the \emph{Network Info} button.
    \item Note the IP neighbor starting with \emph{192.168.42.*}
  \end{enumerate}
  \item Connect to the Raspberry Pi via SSH. To do so, you can install a SSH client app from the Play Store, e.g., \emph{JuiceSSH}, as illustrated in \autoref{fig:juice-ssh}. Use the app as illustrated in Figures \ref{fig:open-juice-ssh}, \ref{fig:rpi-info-connect}, \ref{fig:accept-connection} and \ref{fig:rpi-password}:
  \begin{enumerate}
    \item Open \emph{JuiceSSH} app.
    \item Click on \emph{Quick Connect} button to connect to a new host.
    \item Select \emph{SSH} connection type.
    \item Write the Raspberry Pi information (\emph{pi@hostname:22})\footnote{Replace \emph{hostname} with the IP of the Raspberry Pi.}.
    \item Click on the \emph{OK} button.
    \item Click on the \emph{ACCEPT} button, to accept the key from the Raspberry Pi.
    \item Enter the password of the Raspberry Pi\footnote{The default password is \emph{raspberry}.}.
    \item Click on the \emph{OK} button.
  \end{enumerate}
\end{enumerate}

\begin{figure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/settings.jpg}
    \caption{Settings}
    \label{fig:setting}  
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/wireless-networks.jpg}
    \caption{Wireless \& networks}
    \label{fig:wireless-networks}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/tethering.jpg}
    \caption{Tethering \& portable hotspot}
    \label{fig:tethering}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/ping-net.jpg}
    \caption{Ping \& Net - Play Store}
    \label{fig:ping-net-play-store}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/open-ping-net.jpg}
    \caption{Ping \& Net app}
    \label{fig:ping-net}  
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/find-network.jpg}
    \caption{PIng \& Net - Find network}
    \label{fig:find-network}
  \end{subfigure}
  \caption{Enabling Ethernet-over-USB on Android (pt1)}
  \label{fig:ethernet-over-usb-android-pt1}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/juice-ssh.jpg}
    \caption{Juice SSH - Play Store}
    \label{fig:juice-ssh}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}\ContinuedFloat
    \centering
    \includegraphics[width=0.9\linewidth]{figures/open-juice-ssh.jpg}
    \caption{Juice SSH app}
    \label{fig:open-juice-ssh}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/rpi-info-connect.jpg}
    \caption{Host information}
    \label{fig:rpi-info-connect}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/accept-connection.jpg}
    \caption{Accept connection}
    \label{fig:accept-connection}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/rpi-password.jpg}
    \caption{Raspberry Pi password}
    \label{fig:rpi-password}
  \end{subfigure}
  \begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=0.9\linewidth]{figures/rpi-connected-android.jpg}
    \caption{Android smartphone connected}
    \label{fig:rpi-connected-android}
  \end{subfigure}
  \caption{Enabling Ethernet-over-USB on Android (pt2)}
  \label{fig:ethernet-over-usb-android-pt2}
\end{figure}

Now you should be connected to the Raspberry Pi via SSH, as illustrated in \autoref{fig:rpi-connected-android}.

\section{GUI Deactivation}

Using the GUI with the Raspberry Pi Zero W slows down the device boot time. With the GUI enabled the Raspberry Pi took up to 165 seconds to boot.
We were able to cut this time in half by disabling the GUI, leading to a boot time of 77 seconds.
The GUI can be disabled by choosing the no-GUI  the command \verb|raspi-config|. The path is \verb|Boot Options > Desktop / CLI > Console|.